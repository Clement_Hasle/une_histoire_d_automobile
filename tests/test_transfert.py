import unittest
import os
import csv
from transfert import TraitementPremiereLigne, TraitementLigne

class TestTransfertFunctions(unittest.TestCase):
    '''
    Création d'un fichier test.csv pour tester les fonctions
    '''
    def setUp(self):
        csvfile = open("test.csv", "w")
        csvfile.write("")
        csvfile.close()
        tableTest = ['']*4
        tableTest[0] = ["address","carrosserie","categorie","couleur","cylindree","date_immat","denomination","energy","firstname","immat","marque","name","places","poids","puissance","type_variante_version","vin"]
        tableTest[1] = ["3822 Omar Square Suite 257 Port Emily, OK 43251","45-1743376","34-7904216","LightGoldenRodYellow","3462","2012-05-03","Enhanced well-modulated moderator","37578077","Jerome","OVC-568","Williams Inc","Smith","32","3827","110","Inc, 92-3625175, 79266482","9780082351764"]
        tableTest[2] = ["974 Byrd Mountains New Jennifer, IL 01612","03-0258606","71-7143342","Snow","4837","2019-06-14","Right-sized secondary array","15895400","Paul","859 GZP","Ross PLC","Olson","9","5870","298","LLC, 37-7112501, 39890658","9781246585674"]
        tableTest[3] = ["604 Willis View Suite 279 Hansenview, NY 26033","26-7045508","16-3556230","CadetBlue","1245","1988-11-29","Balanced intangible portal","99868567","Jose","2-2270C","Gillespie PLC","Gomez","33","3504","406","PLC, 67-9571998, 92859586","9781027382348"]
        with open('test.csv', 'a', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter='|')
            for ligne in tableTest:
                writer.writerow(ligne)


    def test_TraitementPremiereLigne(self):
        premiereLigne = ['']
        premiereLigne = ["adresse_titulaire","nom","prenom","immatriculation","date_immatriculation","vin","marque","denomination_commerciale","couleur","carrosserie","categorie","cylindre","energie","places","poids","puissances","type","variante","version"]
        self.assertEqual(TraitementPremiereLigne(), premiereLigne)

    def test_TraitemantLigne(self):
        deuxiemeLigne = []
        deuxiemeLigne = ["3822 Omar Square Suite 257 Port Emily, OK 43251","Smith","Jerome","OVC-568","03/05/2012","9780082351764","Williams Inc","Enhanced well-modulated moderator","LightGoldenRodYellow","45-1743376","34-7904216","3462","37578077","32","3827","110","Inc","92-3625175","79266482"]
        troisiemeLigne = []
        troisiemeLigne = ["974 Byrd Mountains New Jennifer, IL 01612","Olson","Paul","859 GZP","14/06/2019","9781246585674","Ross PLC","Right-sized secondary array","Snow","03-0258606","71-7143342","4837","15895400","9","5870","298","LLC","37-7112501","39890658"]
        quatriemeLigne = []
        quatriemeLigne = ["604 Willis View Suite 279 Hansenview, NY 26033","Gomez","Jose","2-2270C","29/11/1988","9781027382348","Gillespie PLC","Balanced intangible portal","CadetBlue","26-7045508","16-3556230","1245","99868567","33","3504","406","PLC","67-9571998","92859586"]
        lignes = [''] * 4
        compteur = 0
        with open('test.csv') as csvfile:
            reader = csv.reader(csvfile, delimiter="|")
            for row in reader:
                lignes[compteur] = row
                compteur = compteur +1
        
        self.assertEqual(TraitementLigne(lignes[1]), deuxiemeLigne)
        self.assertEqual(TraitementLigne(lignes[2]), troisiemeLigne)
        self.assertEqual(TraitementLigne(lignes[3]), quatriemeLigne)

    '''
    Suppression du fichier test.csv
    '''
    def tearDown(self):
        os.remove('test.csv')