import csv
import os
import argparse
import datetime
from dictionnaire.correspondanceFormat import TAILLE_FORMAT_PROSIV, TAILLE_FORMAT_FLEETMANAGER, INDICE_TVV, INDICE_DATE, dict_correspondance, correspondanceIndice

"""
Lecture : ouvre et lit ligne par ligne le fichier ProSIV. Pour chaque ligne, les fonctions de traitement 
et d'écriture sont appelées pour transcrire le fichier au fur et à mesure.
IN : chemin du fichier ProSiv à transferer.
"""
def Lecture(cheminFic, delimiteur):
    with open(cheminFic) as fichierProSIV:
        reader = csv.reader(fichierProSIV, delimiter="|")
        premiereligne = True
        for ligneProSIV in reader:
            if(premiereligne):          #On appelle une fonction différente pour créer la première ligne
                premiereligne = False   
                Ecriture(TraitementPremiereLigne(), delimiteur)
            else:
                Ecriture(TraitementLigne(ligneProSIV), delimiteur)

"""
TraitementPremiereLigne :  permet de créer la première ligne du fichier FleetManager à retourner.
OUT : nouvelleLigne est la ligne créee retournée sous forme de tableau de chaine de caractère.
"""
def TraitementPremiereLigne():
    nouvelleLigne = ['']*TAILLE_FORMAT_FLEETMANAGER
    for indicePRO in range(TAILLE_FORMAT_PROSIV):
        indiceFM = correspondanceIndice[indicePRO]
        if(indicePRO == INDICE_TVV):
            nouvelleLigne[indiceFM-1] = dict_correspondance[indicePRO+1][indiceFM]
            nouvelleLigne[indiceFM] = dict_correspondance[indicePRO+1][indiceFM+1]
            nouvelleLigne[indiceFM+1] = dict_correspondance[indicePRO+1][indiceFM+2]
        else:
            nouvelleLigne[indiceFM-1] = dict_correspondance[indicePRO+1][indiceFM]
    return nouvelleLigne

"""
TraitementPremiereLigne :  permet de transcrire une ligne de fichier csv au format ProSIV passée en paramètre
en ligne de fichier csv FleetManager.
IN  : ligneProSIV est la ligne de fichier csv au format ProSIV à transcrire.
OUT : nouvelleLigne est la ligne correspondante créee retournée sous forme de tableau de chaine de caractère.
"""
def TraitementLigne(ligneProSIV):
    nouvelleLigne = [''] * TAILLE_FORMAT_FLEETMANAGER
    indicePRO = 0
    for donnee in ligneProSIV:
        indiceFM = correspondanceIndice[indicePRO]
        if(indicePRO == INDICE_TVV):
            part = donnee.split(',')
            nouvelleLigne[indiceFM-1] = part[0]
            nouvelleLigne[indiceFM] = part[1].replace(' ','')
            nouvelleLigne[indiceFM+1] = part[2].replace(' ','')
        else:
            if(indicePRO == INDICE_DATE):
                current_date = datetime.datetime.strptime(donnee, '%Y-%m-%d')
                nouvelleLigne[indiceFM-1] = current_date.strftime('%d/%m/%Y')
            else:
                nouvelleLigne[indiceFM-1] = donnee
        indicePRO = indicePRO + 1
        
    return nouvelleLigne

"""
Ecriture :  permet d'écrire un ligne dans le fichier résultat à retourner, correspondant au fichier csv
au format FleetManager.
IN  : nouvelleLigne est la ligne sous forme de tableau de chaine de caractère à écrire dans le fichier résulat.csv.
OUT : écriture dans le fichier resultat.csv.
"""
def Ecriture(nouvelleLigne, delimiteur):
    with open('resultat.csv', 'a', newline='') as fichierFleetManager:          #newline : permet de spécifier comment on souhaite sauter des lignes
        writer = csv.writer(fichierFleetManager, delimiter=delimiteur)
        writer.writerow(nouvelleLigne)

"""
VerificationFichier :  vérifie que le fichier passé en paramètre existe bien, est bien un fichier et non vide.
IN  : cheminFic est le chemin du fichier à tester.
"""
def VerificationFichier(cheminFic):
    if(not(os.path.exists(cheminFic))):
        print("Le fichier n'existe pas !")
        return False
    if(not(os.path.isfile(cheminFic))):
        print("Le fichier potentiel n'en est pas un !")
        return False
    if(os.path.getsize(cheminFic)<=10):
        print("Le fichier est vide !")
        return False
    return True

"""
VerificationFichier :  vérifie que le délimiteur passé en paramètre est valide.
IN  : delimiteur est le délimiteur à passer en paramètre lors de l'appel du programme.
"""
def CheckDelimiteur(delimiteur):
    if delimiteur is not None:
        if isinstance(delimiteur, str) and len(delimiteur) == 1:
            return delimiteur
        raise argparse.ArgumentTypeError("Le delimiteur n'est pas un caractère")
    return ';'


if __name__=="__main__":
    parser = argparse.ArgumentParser(description="This is a useful description")
    parser.add_argument("cheminFichier", help="recupere le chemin du fichier")
    parser.add_argument("delimiteur", help="recupere le delimiteur", type=CheckDelimiteur)
    args = parser.parse_args()
    with open('resultat.csv', 'w') as csvfile:
        csvfile.write("")
    if(VerificationFichier(args.cheminFichier)):
        Lecture(args.cheminFichier, args.delimiteur)
    else:
        print("Fin de programme")