'''
Ce module propose le dictionnaire de correspondance des formats ProSIV et FleetManager
ainsi que les différentes constantes utiles associées.
'''

#Dictionnaire des correspondance entre le format ProSIV et FleetManager
dict_correspondance = {
    1: {1 : "adresse_titulaire"}, 
    2: {10: "carrosserie"},
    3: {11: "categorie"},
    4: {9: "couleur"},
    5: {12: "cylindre"},
    6: {5: "date_immatriculation"},
    7: {8: "denomination_commerciale"},
    8: {13: "energie"},
    9: {3: "prenom"},
    10: {4: "immatriculation"},
    11: {7: "marque"},
    12: {2: "nom"},
    13: {14: "places"},
    14: {15: "poids"},
    15: {16: "puissances"},
    16: {17: "type", 18: "variante", 19: "version"},
    17: {6: "vin"}
}

#Tableau des correspondance des indices du format ProSIV et FleetManager
correspondanceIndice = [1,10,11,9,12,5,8,13,3,4,7,2,14,15,16,17,6]

#Taille des lignes de chaque format
TAILLE_FORMAT_PROSIV = 17
TAILLE_FORMAT_FLEETMANAGER = 19

#Position de type-variante-version dans le format ProSIV
INDICE_TVV = 15

#Position de date dans le format ProSIV
INDICE_DATE = 5